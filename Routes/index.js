/**
 * Created by shahab on 10/7/15.
 */
'use strict';
var CustomerRoute = require('./CustomerRoute');
var AdminRoute = require('./AdminRoute');
var ManagerRoute = require('./ManagerRoute');
var GuardRoute = require('./GuardRoute');
var SiteRoute = require('./SiteRoute');

var all = [].concat(CustomerRoute,AdminRoute, ManagerRoute,GuardRoute,SiteRoute);

module.exports = all;

