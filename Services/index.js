/**
 * Created by shahab on 10/7/15.
 */
module.exports = {
    CustomerService : require('./CustomerService'),
    ManagerService : require('./ManagerService'),
    AdminService : require('./AdminService'),
    AppVersionService : require('./AppVersionService'),
    GuardService : require('./GuardService'),
    SiteService : require('./SiteServices')
};