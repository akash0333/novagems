'use strict';
/**
 * Created by shahab on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/api/site/add',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.addSite(userData,userPayload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Add  Site',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            payload: {
                maxBytes: 2000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    shortDescription: Joi.string().trim().required(),
                    contactNumber: Joi.string().trim().required(),
                    siteInstructionArray:Joi.array().items(Joi.string().required()).required(),
                    jobDuties: Joi.array().items(
                        Joi.object().keys({
                            title: Joi.string().trim().required(),
                            reminderTime: Joi.string().required()
                        })
                    ).required(),
                    checkoutPoint: Joi.array().items(
                        Joi.object().keys({
                            title: Joi.string().trim().required(),
                            locationLong: Joi.string().required(),
                            locationLat: Joi.string().required(),
                            intervalTime: Joi.string().required()
                        })
                    ).required(),
                    guardGender: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.GENDER.MALE,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.GENDER.FEMALE,UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.GENDER.BOTH]),

                    requiredCert:Joi.array().items(Joi.string().required()).required(),
                    siteStartDate: Joi.date().required().description('In ISO Format'),
                    timeZoneId: Joi.string().required(),
                    timeLotForGuard: Joi.array().items(
                        Joi.object().keys({
                            isReportingPending:Joi.boolean().required().description('If Auto Approve field then value true otherwise false'),
                            startDateTime: Joi.date().required().description('In ISO Format'),
                            endDateTine: Joi.date().required().description('In ISO Format'),
                            startTime: Joi.string().trim().required().description('24 Hrs time format, Eg : 0900 or 2359'),
                            endTime: Joi.string().required().description('24 Hrs time format, Eg : 0900 or 2359'),
                            requiredGuard: Joi.number().integer().required()
                        })
                    ).required(),
                    isAutoApprove: Joi.boolean().required(),
                    siteInstructionImages: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/site/deleteSiteInstruction',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.deleteSiteInstruction(userData,queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Delete Site Instruction',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            validate: {
                payload: {
                    siteId: Joi.string().required(),
                    siteInstructionIndex : Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/site/deleteSiteImage',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.deleteSiteImage(userData,queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Delete Site Image',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            validate: {
                payload: {
                    siteId: Joi.string().required(),
                    siteImageIndex : Joi.number().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/site/deleteSite',
        handler: function (request, reply) {
            var queryData = request.query;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.deleteSite(userData,queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Delete Site',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            validate: {
                query: {
                    siteId: Joi.string().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/site/assignGuardToSite',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.assignGuardToSite(userData,queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Assign Guard To Site',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            validate: {
                payload: {
                    siteId: Joi.string().required().description('Mongo Id of Site'),
                    timeSlotId: Joi.string().required().description('Mongo Id of inside time slot'),
                    guardId : Joi.string().required().description('Mongo Id of guard')
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/site/getSiteList',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.ManagerController.getSiteList(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Site List added by manager',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    limit: Joi.number().integer().optional(),
                    skip: Joi.number().integer().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/site/getSitePendingList',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.getSitePendingList(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Site List added by manager',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    limit: Joi.number().integer().optional(),
                    skip: Joi.number().integer().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/site/getSiteArchiveList',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.getSiteArchiveList(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Site ArchiveList List added by manager',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    limit: Joi.number().integer().optional(),
                    skip: Joi.number().integer().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/site/getSiteDetails',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.ManagerController.getSiteDetails(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Site Details',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    siteId: Joi.string().required().description('Mongo Id of Site')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/site/getWatchSiteList',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.ManagerController.getCurrentSiteList(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Current Watch Site List added by manager',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    limit: Joi.number().integer().optional(),
                    skip: Joi.number().integer().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/site/makeSiteFlagReport',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.makeSiteFlagReport(userData,queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Make Guard Flag Report To Site',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            validate: {
                payload: {
                    siteId: Joi.string().required().description('Mongo Id of Site'),
                    guardId: Joi.string().required().description('Mongo Id of Guard'),
                    siteFlagReport:Joi.boolean().required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/site/approveReportForSite',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.approveReportForSite(userData,queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Approv Report for Guard To Site',
            auth: 'ManagerAuth',
            tags: ['api', 'site'],
            validate: {
                payload: {
                    siteId: Joi.string().required().description('Mongo Id of Site'),
                    timeObjectId:Joi.array().items(Joi.string().required()).required(),
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/site/getScheduleSiteListWeek',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.getScheduleSiteListWeek(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Site Schedule Site List added by manager',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query:{
                    weekFilterDate:Joi.date().iso().optional().allow('').description('Date should be in iso format'),
                    limit: Joi.number().integer().optional(),
                    skip: Joi.number().integer().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/api/site/getScheduleSiteListMonth',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            Controller.SiteController.getScheduleSiteListMonth(request.query,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get Site Schedule month Site List added by manager',
            tags: ['api', 'site'],
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    monthFilterDate:Joi.date().iso().optional().allow('').description('Date should be in iso format'),
                    limit: Joi.number().integer().optional(),
                    skip: Joi.number().integer().optional()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
];
