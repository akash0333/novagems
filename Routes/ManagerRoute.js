'use strict';
/**
 * Created by shahab on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'GET',
        path: '/api/Manager/getResetPasswordToken',
        handler: function (request, reply) {
            var email = request.query.email;
            Controller.ManagerController.getResetPasswordToken(email, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Sends Reset Password Token To User',
            tags: ['api', 'Manager'],
            validate: {
                query: {
                    email: Joi.string().email().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/Manager/register',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.ManagerController.createManager(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Register Manager',
            tags: ['api', 'Manager'],
            payload: {
                maxBytes: 2000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    name: Joi.string().regex(/^[a-zA-Z ]+$/).trim().required(),
                    countryCode: Joi.string().max(4).required().trim(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                    facebookId: Joi.string().optional(),
                    email: Joi.string().email().required(),
                    password: Joi.string().optional().min(5).trim(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    appVersion: Joi.string().required().trim(),
                    profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .required()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/Manager/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.ManagerController.loginManager(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Email & Password For  Customer',
            tags: ['api', 'Manager'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().required().min(5).trim(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    flushPreviousSessions: Joi.boolean().required(),
                    appVersion: Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/changePassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.ManagerController.changePassword(queryData, userData, function (err, data) {
                console.log('error', err, data)
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'Change Password Manager',
        tags: ['api', 'Manager'],
        auth: 'ManagerAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(5).trim(),
                newPassword: Joi.string().required().min(5).trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT',
        path: '/api/Manager/firstTimeChangePassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.ManagerController.firstTimeChangePassword(queryData, userData, function (err, data) {
                console.log('error', err, data)
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'Change First Time  Password Manager',
        tags: ['api', 'Manager'],
        auth: 'ManagerAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                newPassword: Joi.string().required().min(5).trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT',
        path: '/api/Manager/resetPassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.ManagerController.resetPassword(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Reset Password For Customer',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    passwordResetToken: Joi.string().required(),
                    newPassword : Joi.string().min(5).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/resendOTP',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.ManagerController.resendOTP(userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Resend OTP for Manager',
            tags: ['api', 'Manager'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/resendEmailVerificationToken',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.ManagerController.resendEmailVerificationToken(userData.email, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Resend Email Verification Token for Manager',
            tags: ['api', 'Manager'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/verifyOTP',
        handler: function (request, reply) {
            var OTP = request.query.OTP;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.ManagerController.verifyOTP(OTP,userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            description: 'Verify OTP for Manager',
            tags: ['api', 'Manager'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                query: {
                    OTP: Joi.string().length(4).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/verifyEmail/{emailVerificationToken}',
        handler: function (request, reply) {
            var resetToken = request.params.emailVerificationToken;
            Controller.ManagerController.verifyEmail(resetToken, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            description: 'Verify Email for Manager',
            tags: ['api', 'Manager'],
            validate: {
                params: {
                    emailVerificationToken: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/updateBankAccountDetails',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.ManagerController.updateBankAccountDetails(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Manager update bank account details',
            auth: 'UserAuth',
            tags: ['api', 'Manager'],
            validate: {
                payload: {
                    CLABE: Joi.string().required().trim().min(2),
                    nameInBankAccount: Joi.string().regex(/^[a-zA-Z ]+$/).trim().required(),
                    routingNumber: Joi.string().required().trim().min(9),
                    accountNumber: Joi.string().required().trim().min(5)
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/addGuard',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.GuardController.createGuard(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Add Guard || WARNING : Will not work from documentation, use postman instead',
            auth: 'ManagerAuth',
            tags: ['api', 'Manager'],
            payload: {
            maxBytes: 2000000,
            parse: true,
            output: 'file'
            },
            validate: {
            payload: {
                email: Joi.string().email().required(),
                name: Joi.string().required(),
                userName: Joi.string().required(),
                countryCode: Joi.string().max(4).required().trim(),
                phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                address: Joi.string().required(),
                basicSecurityLicence:Joi.string().required(),
                advancedSecurityLicence: Joi.string().required(),
                class5Licence:Joi.string().required(),
                class7Licence: Joi.string().required(),
                firstAidL1: Joi.string().required(),
                firstAidL2: Joi.string().required(),
                profilePic: Joi.any()
                    .meta({swaggerType: 'file'})
                    .optional()
                    .description('image file')
            },
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                payloadType: 'form',
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'PUT',
        path: '/api/Manager/logout',
        handler: function (request, reply) {
            var token = request.auth.credentials.token;
            var userData = request.auth.credentials.userData;
            if (!token){
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN));
            }else if(userData && userData.type != UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.Manager){
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED))
            }else {
                Controller.ManagerController.logoutManager(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess())
                    }
                });

            }
        },
        config: {
            auth: 'ManagerAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Logout Manager',
            tags: ['api', 'Manager'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/Manager/tokenLogin',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.ManagerController.tokenLoginManager(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Token Login Manager',
            auth: 'ManagerAuth',
            tags: ['api', 'Manager'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }

    }
];