var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var Config = require('../Config');
var connection = mongoose.createConnection(Config.dbConfig.mongo.URI);
autoIncrement.initialize(connection);


var jobDutiesArray = new Schema({
    title:{type: String},
    reminderTime:{type: String}
});

var checkoutPointArray = new Schema({
    title:{type: String},
    locationLong: {type: String},
    locationLat: {type: String},
    intervalTime:{type: String}
});

var timeLotForGuard = new Schema({
    startDateTime : {type : Date}, // IN ISO Format
    endDateTime : {type : Date}, // IN ISO Format
    startTime:{type: String},
    endTime:{type: String},
    requiredGuard:{type: Number},
    guardId:[{type: Schema.ObjectId, ref: 'Guard', default: null}],
    isReportingPending: {type: Boolean, default: false, required: true}

});





var Site = new Schema({
    managerId:   { type: Schema.ObjectId, ref: 'Manager', required: true},
    title: {type: String,default: ''},
    shortDescription : {type: String,default: ''},
    contactNumber: {type: String,default: ''},
    siteInstructionArray:  {type: [], default: []  },
    siteInstructionImage:{type: [], default: []  },
    jobDuties:[jobDutiesArray],
    checkoutPoint:[checkoutPointArray],
    guardGender: { type: String,
        enum: [Config.APP_CONSTANTS.DATABASE.GENDER.MALE,Config.APP_CONSTANTS.DATABASE.GENDER.FEMALE,
            Config.APP_CONSTANTS.DATABASE.GENDER.BOTH]
    },
    requiredCert :{type: [], default: [] },
    siteStartDate: {type: Date},
    timeZoneId: {  type: String, default: '' },
    timeLotForGuard:[timeLotForGuard],
    siteColorStatus: {type: String, default: Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.GREEN,
        enum: [
            Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.GREEN, Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.RED,
            Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.YELLOW]
    },
    siteScheduleColorStatus: {type: String, default: Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.RED,
        enum: [
            Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.GREEN, Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.RED,
            Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.YELLOW]
    },
    siteStatus: {type: String, default: Config.APP_CONSTANTS.DATABASE.SITE_STATUS.NOT_COMPLETED,
        enum: [
            Config.APP_CONSTANTS.DATABASE.SITE_STATUS.NOT_COMPLETED, Config.APP_CONSTANTS.DATABASE.SITE_STATUS.COMPLETED]
    },
    reportingLeftCount:{ type: Number},
    isAutoApprove: {type: Boolean, default: false, required: true},
    isCompleteAdded: {type: Boolean, default: false, required: true},
    isArchive: {type: Boolean, default: false, required: true},
    reportStatus: {type: Boolean, default: false, required: true},
    createdAt: { type: Date,default: Date.now},
    modifiedAt: { type: Date}
});

Site.plugin(autoIncrement.plugin, {model: 'Site', field: 'siteId'});

module.exports = mongoose.model('Site', Site);