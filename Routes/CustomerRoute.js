'use strict';
/**
 * Created by shahab on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/api/customer/register',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.createCustomer(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201)
                }
            });
        },
        config: {
            description: 'Register Customer',
            tags: ['api', 'customer'],
            payload: {
                maxBytes: 2000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    name: Joi.string().regex(/^[a-zA-Z ]+$/).trim().min(2).required(),
                    countryCode: Joi.string().max(4).required().trim(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                    email: Joi.string().email().required(),
                    password: Joi.string().optional().min(5).trim(),
                    facebookId: Joi.string().optional().trim(),
                    language: Joi.string().required().valid([
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX]),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    appVersion: Joi.string().required().trim(),
                    profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .required()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType : 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/loginViaFacebook',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.loginCustomerViaFacebook(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Facebook For  Customer',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    facebookId: Joi.string().required(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    language: Joi.string().required().valid([
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX]),
                    deviceToken: Joi.string().required().trim(),
                    flushPreviousSessions: Joi.boolean().required(),
                    appVersion: Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/login',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.loginCustomer(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Email & Password For  Customer',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().required().min(5).trim(),
                    deviceType: Joi.string().required().valid([UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    language: Joi.string().required().valid([
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX]),
                    deviceToken: Joi.string().required().trim(),
                    flushPreviousSessions: Joi.boolean().required(),
                    appVersion: Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/changePassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.CustomerController.changePassword(queryData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED))
                }
            });
        }, config: {
        description: 'Change Password Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(5).trim(),
                newPassword: Joi.string().required().min(5).trim()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
            }
        }
    }
    },
    {
        method: 'GET',
        path: '/api/customer/getResetPasswordToken',
        handler: function (request, reply) {
            var email = request.query.email;
            Controller.CustomerController.getResetPasswordToken(email, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Sends Reset Password Token To User',
            tags: ['api', 'customer'],
            validate: {
                query: {
                    email: Joi.string().email().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/resetPassword',
        handler: function (request, reply) {
            var queryData = request.payload;
            Controller.CustomerController.resetPassword(queryData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Reset Password For Customer',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    passwordResetToken: Joi.string().required(),
                    newPassword : Joi.string().min(5).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/logout',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            if (userData.type == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER){
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNAUTHORIZED));
            }else {
                Controller.CustomerController.logoutCustomer(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess())
                    }
                });
            }
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Logout Customer',
            tags: ['api', 'customer'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/resendOTP',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.CustomerController.resendOTP(userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            description: 'Resend OTP for Customer',
            tags: ['api', 'customer'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/verifyOTP',
        handler: function (request, reply) {
            var queryData = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
            Controller.CustomerController.verifyOTP(queryData, userData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            auth: 'UserAuth',
            description: 'Verify OTP for Customer',
            tags: ['api', 'customer'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                payload: {
                    countryCode: Joi.string().max(4).required().trim(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).required(),
                    OTPCode: Joi.string().length(4).required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/verifyEmail/{emailVerificationToken}',
        handler: function (request, reply) {
            var resetToken = request.params.emailVerificationToken;
            Controller.CustomerController.verifyEmail(resetToken, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess())
                }
            });
        },
        config: {
            description: 'Verify Email for Customer',
            tags: ['api', 'customer'],
            validate: {
                params: {
                    emailVerificationToken: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/api/customer/updateProfile',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.updateCustomer(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Customer Update profile || WARNING : Will not work from documentation, use postman instead',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            payload: {
                maxBytes: 2000000,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    name: Joi.string().optional().min(2),
                    countryCode: Joi.string().max(4).optional().trim(),
                    email: Joi.string().email().optional(),
                    phoneNo: Joi.string().regex(/^[0-9]+$/).min(5).optional(),
                    language: Joi.string().optional().valid([
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
                        UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX]),
                    deviceToken: Joi.string().optional().trim().allow(''),
                    profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/customer/addNewAddress',
        handler: function (request, reply) {
            var userPayload = request.payload;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.addNewAddress(userPayload, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Add New Address For Customer',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    locationLat : Joi.number().required(),
                    locationLong : Joi.number().required(),
                    streetAddress: Joi.string().required().trim(),
                    state: Joi.string().required().min(2).trim(),
                    apartmentSuite: Joi.string().required().min(1).trim(),
                    nickName: Joi.string().required().min(2).trim(),
                    city: Joi.string().required().min(2).trim(),
                    country: Joi.string().required().min(2).trim(),
                    directionDetails: Joi.string().optional().min(3).trim(),
                    zip: Joi.string().required().min(3).trim()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/api/customer/removeAddress',
        handler: function (request, reply) {
            var addressId = request.query.addressId;
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id && addressId) {
                Controller.CustomerController.removeAddress(addressId, userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess())
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Remove Address For Customer',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            validate: {
                query: {
                    addressId : Joi.string().min(3).required()
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/customer/getAllAddress',
        handler: function (request, reply) {
            var userData = request.auth && request.auth.credentials && request.auth.credentials.userData;
            if (userData && userData.id) {
                Controller.CustomerController.getAddress(userData, function (err, data) {
                    if (err) {
                        reply(UniversalFunctions.sendError(err));
                    } else {
                        reply(UniversalFunctions.sendSuccess(null, data))
                    }
                });
            } else {
                reply(UniversalFunctions.sendError(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR));
            }
        },
        config: {
            description: 'Get All Addresses of Customer',
            auth: 'UserAuth',
            tags: ['api', 'customer'],
            validate: {
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];