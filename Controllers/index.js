/**
 * Created by shahab on 10/7/15.
 */
module.exports  = {
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    CustomerController : require('./CustomerController'),
    ManagerController : require('./ManagerController'),
    GuardController : require('./GuardController'),
    SiteController : require('./SiteController')
};