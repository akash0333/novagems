var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Guard = new Schema({
    managerId: {type: Schema.ObjectId, ref: 'Manager', default: null},
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    countryCode: {type: String, required: true, trim: true, min:2, max:5},
    phoneNo: {type: String, required: true, trim: true, index: true, unique: true, min: 5, max: 15},
    newNumber: {type: String, trim: true, sparse: true, index: true, unique: true, min: 5, max: 15},
    email: {type: String, trim: true, unique: true, index: true, required: true},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    firstTimeLogin: {type: Boolean, default: false},
    language: {
        type: String, required: true,
        default : Config.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX,
        enum: [
            Config.APP_CONSTANTS.DATABASE.LANGUAGE.EN,
            Config.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX
        ]
    },
    password: {type: String},
    passwordResetToken: {type: String, trim: true, unique: true, index: true, sparse: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    appVersion: {type: String},
    OTPCode: {type: String, trim: true, unique: true, sparse: true, index: true},
    accessToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    emailVerificationToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    deviceToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    siteFlagReport: {type: Boolean, default: true, required: true},
    isBlocked: {type: Boolean, default: false, required: true},
    emailVerified: {type: Boolean, default: false, required: true},
    phoneVerified: {type: Boolean, default: false, required: true},
    currentLocation: {type: [Number], index: '2d'},
    siteColorStatus: {type: String, default: Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.GREEN,
        enum: [
            Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.GREEN, Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.RED,
            Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.YELLOW]
    },
    profilePicURL: {
        original: {type: String, default: ""},
        thumbnail: {type: String, default: ""}
    },
    basicSecurityLicence: {type: String, default: ""},
    advancedSecurityLicence: {type: String, default: ""},
    class5Licence: {type: String, default: ""},
    class7Licence: {type: String, default: ""},
    firstAidL1: {type: String, default: ""},
    firstAidL2: {type: String, default: ""}
});

Guard.index({'currentLocation.coordinates': "2d"});


module.exports = mongoose.model('Guard', Guard);