'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');

function clearDeviceTokenFromDB(token,cb){

    if (token) {
        var criteria = {
            deviceToken: token
        };
        var setQuery = {
            $unset: {deviceToken: 1}
        };
        var options = {
            multi: true
        };
        Service.GuardService.updateGuard(criteria, setQuery, options, cb)
    } else {
        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }
}

var createGuard = function (payloadData,userData, callback) {
    console.log('sending',payloadData)
    var accessToken = null;
    var uniqueCode = null;
    payloadData.password = UniversalFunctions.generateRandomString();
    var dataToSave = payloadData;
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    dataToSave.firstTimeLogin = false;
    var customerData = null;
    var dataToUpdate = {};
    if (payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: "",
            thumbnail: ""
        }
    }

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            if (dataToSave.phoneNo && dataToSave.phoneNo.split('')[0] == 0) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            var criteria = {
                deviceToken: dataToSave.deviceToken
            };
            var setQuery = {
                $unset: {deviceToken: 1}

            };
            var options = {
                multi: true
            };
            Service.GuardService.updateGuard(criteria, setQuery, options, cb)
        },
        function (cb) {
            //Insert Into DB
            dataToSave.managerId = userData._id;
            dataToSave.newNumber = payloadData.phoneNo;
            dataToSave.registrationDate = new Date().toISOString();
            Service.GuardService.createGuard(dataToSave, function (err, customerDataFromDB) {
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('guards.$phoneNo_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('guards.$email_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            if (customerData && customerData._id && payloadData.profilePic && payloadData.profilePic.filename) {
                UploadManager.uploadFile(payloadData.profilePic, customerData._id, "GUARD",function (err, uploadedInfo) {
                    console.log('update profile pic',err,uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL.original = uploadedInfo.original || null;
                        dataToUpdate.profilePicURL.thumbnail =uploadedInfo.original || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
        function (cb) {
            if (customerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update User
                var criteria = {
                    _id: customerData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.GuardService.updateGuard(criteria, setQuery, {new: true}, function (err, updatedData) {
                    customerData = updatedData;
                    cb(err, updatedData)
                })
            }else {
                if (customerData && customerData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original){
                    var criteria = {
                        _id: customerData._id
                    };
                    Service.GuardService.deleteGuard(criteria,function (err, updatedData) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                }else {
                    cb();
                }
            }
        },
        //function (cb) {
        //    //Send Verification Email
        //    if (customerData) {
        //        var emailType = 'REGISTRATION_MAIL';
        //        var variableDetails = {
        //            user_name: dataToSave.name,
        //            verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/customer/verifyEmail/' + dataToSave.emailVerificationToken
        //        };
        //        var emailAddress = dataToSave.email;
        //        NotificationManager.sendEmailToUser(emailType, variableDetails, emailAddress, cb)
        //    } else {
        //        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
        //    }
        //
        //},
        function (cb) {
            //Set Access Token
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.GUARD
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            var criteria = {
                email: customerData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.GuardService.getGuard(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    customerData = result && result[0] || null;
                    cb();
                }
            });

        }
    ], function (err, data) {

        if (err) {
            callback(err);
        } else {
            console.log("customerData",customerData)
            delete customerData['__v'];
            callback(null, {
                accessToken: accessToken,
                userDetails:customerData
            });
        }
    });
};

var loginGuard = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    async.series([
        function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.GuardService.getGuard(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payloadData.password)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            if (userFound && payloadData.deviceToken != userFound.deviceToken && !flushPreviousSessions){
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACTIVE_PREVIOUS_SESSIONS)
            } else {
                var criteria = {
                    deviceToken: payloadData.deviceToken
                };
                var setQuery = {
                    $unset: {deviceToken: 1}
                };
                var options = {
                    multi: true
                };
                Service.GuardService.updateGuard(criteria, setQuery, options, cb)
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.GuardService.updateGuard(criteria, setQuery, {new:true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });

        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.Guard
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken){
                            accessToken = output && output.accessToken;
                            cb();
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {accessToken: accessToken, userDetails: UniversalFunctions.deleteUnnecessaryUserData(updatedUserDetails)});
        }
    });
};

var logoutGuard = function (userData, callback) {
    if (!userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userId = userData && userData.id || 1;

        async.series([
            function (cb) {
                //Check if the Guard is free or not
                Service.GuardService.getGuard({_id : userData.id}, {availabilityStatus : 1},{lean:true}, function (err, GuardAry) {
                    if (err){
                        cb(err)
                    }else if (GuardAry && GuardAry[0]){
                        if (GuardAry[0].availabilityStatus){
                            cb()
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.CANNOT_LOGOUT)
                        }
                    }else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userId
                };
                var setQuery = {
                    $unset: {
                        accessToken: 1
                    }
                };
                var options = {};
                Service.GuardService.updateGuard(criteria, setQuery, options, cb);
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var updateGuard = function (userPayload,userData, callback) {
    var dataToUpdate = {};
    var updatedUser = null;
    if (!userPayload || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var samePhoneNo = (userData.phoneNo == userPayload.phoneNo) || false;
        if (userPayload.name && userPayload.name!='') {
            dataToUpdate.name = UniversalFunctions.sanitizeName(userPayload.name);
        }
        if (userPayload.deviceToken && userPayload.deviceToken!='') {
            dataToUpdate.deviceToken = userPayload.deviceToken;
        }
        if (userPayload.phoneNo && userPayload.phoneNo!='') {
            dataToUpdate.phoneNo = userPayload.phoneNo;
        }
        if (userPayload.profilePic && userPayload.profilePic.filename) {
            dataToUpdate.profilePicURL = {
                original: null,
                thumbnail: null
            }
        }
        if (Object.keys(dataToUpdate).length == 0){
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOTHING_TO_UPDATE);
        }else {
            async.series([
                function (cb) {
                    //Check all empty values validations
                    if (userPayload.name && !dataToUpdate.name){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMPTY_VALUE)
                    }else if (dataToUpdate.name){
                        UniversalFunctions.customQueryDataValidations('NAME','name',dataToUpdate.name, cb)
                    }else {
                        cb();
                    }

                },
                function (cb) {
                    //Check if profile pic is being updated
                    if (userPayload.profilePic && userPayload.profilePic.filename) {
                        UploadManager.uploadFileToS3WithThumbnail(userPayload.profilePic, userData.id, function (err, uploadedInfo) {
                            console.log('err in profile pic',err,uploadedInfo)
                            if (err) {
                                cb(err)
                            } else {
                                dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                                dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                                cb();
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (userPayload.phoneNo && !samePhoneNo) {
                        var criteria = {
                            phoneNo: userPayload.phoneNo
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            } else {
                                if (result && result.length > 0) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST)
                                } else {
                                    cb();
                                }
                            }
                        });
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Update User
                    var criteria = {
                        _id: userData.id
                    };
                    var setQuery = {
                        $set: dataToUpdate
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {new:true}, function (err, updatedData) {
                        updatedUser = updatedData;
                        cb(err,updatedData)
                    })
                }
            ], function (err, result) {
                callback(err, {userData : UniversalFunctions.deleteUnnecessaryUserData(updatedUser)});
            })
        }
    }
};

var resetPassword = function (payloadData, callback) {
    var userObj = null;
    if (!payloadData || !payloadData.email || !payloadData.passwordResetToken || !payloadData.newPassword) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    email: payloadData.email
                };
                Service.GuardService.getGuard(criteria, {}, {lean: true}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            userObj = userData && userData[0] || null;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (userObj) {
                    if (userObj.passwordResetToken != payloadData.passwordResetToken) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RESET_PASSWORD_TOKEN);
                    } else {
                        cb();
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                }
            },
            function (cb) {
                if (userObj) {
                    var criteria = {
                        email: payloadData.email
                    };
                    var setQuery = {
                        password: UniversalFunctions.CryptData(payloadData.newPassword),
                        $unset: {passwordResetToken: 1}
                    };
                    Service.GuardService.updateGuard(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};


var changePassword = function (queryData,userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id : userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.GuardService.getGuard(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)){
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                }else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)){
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                }else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin : false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.GuardService.updateGuard(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};

var verifyEmail = function (emailVerificationToken, callback) {
    if (!emailVerificationToken){
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }else {
        var userData = null;
        async.series([
            function (cb) {
                var criteria = {
                    emailVerificationToken : emailVerificationToken
                };
                var setQuery  = {
                    $set : {emailVerified: true},
                    $unset : {emailVerificationToken : 1}
                };
                var options = {new:true};
                Service.GuardService.updateGuard(criteria,setQuery,options, function (err, updatedData) {
                    if (err){
                        cb(err)
                    }else {
                        if (!updatedData){
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        }else {
                            userData = updatedData;
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err){
                callback(err)
            }else {
                callback(null,UniversalFunctions.deleteUnnecessaryUserData(userData));
            }

        });
    }

};

var verifyOTP = function (OTP, userData, callback) {
    if (!OTP || !userData || !userData.id){
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }else {
        async.series([
            function (cb) {
                //Check verification code :
                if (OTP == userData.OTPCode){
                    cb();
                }else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                }
            },
            function (cb) {
                var criteria = {
                    _id : userData.id,
                    OTPCode : OTP
                };
                var setQuery  = {
                    $set : {phoneVerified : true},
                    $unset : {OTPCode : 1}
                };
                if (!userData.phoneVerified && userData.newNumber){
                    setQuery.$set.phoneNo = userData.newNumber;
                    setQuery.$unset.newNumber = 1;
                }
                var options = {new:true};
                Service.GuardService.updateGuard(criteria,setQuery,options, function (err, updatedData) {
                    if (err){
                        cb(err)
                    }else {
                        if (!updatedData){
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        }else {
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err){
                callback(err)
            }else {
                callback();
            }

        });
    }

};

var updateBankAccountDetails = function (userPayload, userData, callback) {
    if (!userData || !userData.id){
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }else {
            var criteria = {
                _id : userData.id
            };
            var setQuery  = {
                $set : {
                    paymentDetails : userPayload,
                    registrationCompleted : true
                }
            };
            var options = {new:true};
            Service.GuardService.updateGuard(criteria,setQuery,options, function (err, updatedData) {
                if (err){
                    callback(err)
                }else {
                    if (!updatedData){
                        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                    }else {
                        callback(null,{userDetails : updatedData});
                    }
                }
            });
    }
};

var resendOTP = function (userData, callback) {
    /*
     Create a Unique 4 digit code
     Insert It Into  DB
     Send the 4 digit code via SMS
     Send Back Response
     */
    var phoneNo = userData.newNumber || userData.phoneNo;
    var countryCode = userData.countryCode;
    if (!phoneNo) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            function (cb) {
                CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.Guard,function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode,
                        codeUpdatedAt: new Date().toISOString()
                    }
                };
                var options = {
                    lean: true
                };
                Service.GuardService.updateGuard(criteria, setQuery, options, cb);
            }, function (cb) {
                //Send SMS to User
                NotificationManager.sendSMSToUser(uniqueCode, countryCode, phoneNo, function (err, data) {
                    cb();
                })
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var resendEmailVerificationToken = function (email, callback) {
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userFound = null;
        var emailVerificationToken = null;
        async.series([
            function (cb) {
                //verify email address
                if (!UniversalFunctions.verifyEmailFormat(email)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                } else {
                    cb();
                }
            },
            function (cb) {
                var criteria = {
                    email: email
                };
                var projection = {};
                var option = {
                    lean: true
                };
                Service.GuardService.getGuard(criteria, projection, option, function (err, result) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = result && result[0] || null;
                        cb();
                    }
                });

            },
            function (cb) {
                //validations
                if (!userFound) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                } else {
                    emailVerificationToken = UniversalFunctions.CryptData(JSON.stringify(new Date().toISOString()));
                    cb();
                }
            },
            function (cb) {
                //Update emailverification token in DB
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    emailVerificationToken : emailVerificationToken
                };
                var option = {
                    new: true
                };
                Service.GuardService.updateGuard(criteria, setQuery, option, function (err, updatedData) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = updatedData;
                        cb();
                    }
                });            },
            function (cb) {
                //Send Verification Email
                if (userFound) {
                    var emailType = 'REGISTRATION_MAIL';
                    var variableDetails = {
                        user_name: userFound.name,
                        verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/Guard/verifyEmail/' + emailVerificationToken
                    };
                    NotificationManager.sendEmailToUser(emailType, variableDetails, email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }

            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var getResetPasswordToken = function (email, callback) {
    var generatedString = UniversalFunctions.generateRandomString();
    generatedString = UniversalFunctions.CryptData(generatedString);
    var userObj = null;
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //update user
                var criteria = {
                    email: email
                };
                var setQuery = {
                    passwordResetToken: generatedString
                };
                Service.GuardService.updateGuard(criteria, setQuery, {new: true}, function (err, userData) {
                    console.log('update user', err, userData, !userData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            userObj = userData;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (userObj) {
                    var variableDetails = {
                        user_name: userObj.name,
                        password_reset_token: userObj.passwordResetToken,
                        password_reset_link: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME +
                        + '/GuardResetPassword.html?passwordResetToken=' + generatedString + '&email=' + userObj.email
                    };
                    NotificationManager.sendEmailToUser('FORGOT_PASSWORD', variableDetails, userObj.email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback()
            }
        })
    }
};


module.exports = {
    createGuard: createGuard,
    verifyEmail: verifyEmail,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    getResetPasswordToken: getResetPasswordToken,
    loginGuard: loginGuard,
    logoutGuard: logoutGuard,
    updateBankAccountDetails: updateBankAccountDetails,
    resendEmailVerificationToken: resendEmailVerificationToken,
     changePassword: changePassword,
    resetPassword: resetPassword,
    updateGuard: updateGuard
};