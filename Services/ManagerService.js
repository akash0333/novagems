'use strict';

var Models = require('../Models');

//Get Users from DB
var getManager = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.Manager.find(criteria, projection, options, callback);
};

//var getAddress = function (criteria, projection, options, callback) {
//    options.lean = true;
//    Models.ManagerAddresses.find(criteria, projection, options, callback);
//};
//
//var updateAddress = function (criteria, dataToSet, options, callback) {
//    options.lean = true;
//    options.new = true;
//    Models.ManagerAddresses.findOneAndUpdate(criteria, dataToSet, options, callback);
//};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.Manager.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};

//Insert User in DB
var createManager = function (objToSave, callback) {
    new Models.Manager(objToSave).save(callback)
};

//Update User in DB
var updateManager = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.Manager.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteManager = function (criteria, callback) {
    Models.Manager.findOneAndRemove(criteria, callback);
};

var addAddress = function (objToSave, callback) {
    new Models.ManagerAddresses(objToSave).save(callback)
};

module.exports = {
    getManager: getManager,
    getAllGeneratedCodes: getAllGeneratedCodes,
    updateManager: updateManager,
    //updateAddress: updateAddress,
    //addAddress: addAddress,
    //getAddress: getAddress,
    deleteManager: deleteManager,
    createManager: createManager
};

