'use strict';

var Models = require('../Models');

//Get Users from DB
var getGuard = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.Guard.find(criteria, projection, options, callback);
};

//Insert User in DB
var createGuard = function (objToSave, callback) {
    new Models.Guard(objToSave).save(callback)
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.Guard.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};

//Update User in DB
var updateGuard = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.Guard.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteGuard= function (criteria, callback) {
    Models.Guard.findOneAndRemove(criteria, callback);
};

module.exports = {
    getGuard: getGuard,
    getAllGeneratedCodes: getAllGeneratedCodes,
    updateGuard: updateGuard,
    deleteGuard: deleteGuard,
    createGuard: createGuard
};