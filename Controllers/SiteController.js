'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');
var moment = require('moment');
var _           = require('underscore');

var addSite = function (userData,payloadData, callback) {
    var customerObj = null,siteData = null,mImage = [];
    if (!payloadData && !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        payloadData.managerId = userData.id;
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    _id: userData.id
                };
                Service.ManagerService.getManager(criteria, {}, {lean: true}, function (err, userData1) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData1 || userData1.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData1 && userData1[0] || null;
                            if(customerObj.isBlocked){
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_BLOCK);
                            } else  if(customerObj.isDeleted) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACCOUNT_DELETED);
                            } else {
                                cb()
                            }

                        }
                    }
                })
            },
            function(cb){
                if (payloadData.siteInstructionImages != undefined && payloadData.siteInstructionImages.length > 0 ) {
                    var taskInParallel1 = [];
                    for (var key in payloadData.siteInstructionImages) {
                        (function (key) {
                            taskInParallel1.push((function (key) {
                                return function (embeddedCB) {//TODO
                                    UploadManager.uploadFile(payloadData.siteInstructionImages[key],userData.id,"SITE",function (err, uploadedInfo) {
                                        var original =uploadedInfo.original || null;
                                        mImage.push(original);
                                        embeddedCB();
                                    })
                                }
                            })(key))
                        }(key));
                    }
                    async.parallel(taskInParallel1, function (err, result) {

                        if(err)
                        {
                            cb(err);
                        }
                        else {
                            cb();
                        }
                    });
                } else if(payloadData.siteInstructionImages != undefined){
                    UploadManager.uploadFile(payloadData.siteInstructionImages, userData._id, "SITE",function (err, uploadedInfo) {
                        console.log('update profile pic',err,uploadedInfo)
                        if (err) {
                            cb(err)
                        } else {
                            mImage.push(uploadedInfo.original);
                            cb();
                        }
                    })
                } else {
                    cb();
                }
            },
            function (cb) {
                //Insert Into DB
                if(payloadData.isAutoApprove == false){
                    payloadData.reportingLeftCount = payloadData.timeLotForGuard.length;
                } else {
                    payloadData.reportingLeftCount = 0;
                }
                Service.SiteService.createSite(payloadData, function (err, customerDataFromDB) {
                    if (err) {
                        cb(err)
                    } else {
                        siteData= customerDataFromDB;
                        cb();
                    }
                })
            },
            function(cb){
                console.log("siteData",siteData);
                var criteria ={ "_id":siteData._id};
                var datatoSet ={$addToSet:{siteInstructionImage:{ $each:mImage}}};
                Service.SiteService.updateSite(criteria,datatoSet,{lean: true}, function (err, updatedData) {
                    if (err){
                        return cb(err)
                    }else {
                        if (!updatedData){
                            return  cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                        }
                        cb(null);
                    }
                });
            },
        ], function (err, result) {
            callback(err, null);
        })
    }
};



var deleteSiteInstruction = function (userData,payloadData, callback) {
    //console.log(payloadData);
    var managerObj = null,newSiteInstruction;
    if (!payloadData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    _id: payloadData.siteId
                };
                Service.SiteService.getSite(criteria, {}, {lean: true}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND);
                        } else {
                            managerObj = userData && userData[0] || null;
                            if(typeof managerObj.siteInstructionArray[payloadData.siteInstructionIndex] === 'undefined') {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND);
                            }
                            else {
                                console.log("before",managerObj.siteInstructionArray);
                                managerObj.siteInstructionArray.splice(payloadData.siteInstructionIndex,1);
                                console.log("after",managerObj.siteInstructionArray);
                                cb()
                            }

                        }
                    }
                })
            },
            function (cb) {
                if (managerObj) {
                    var criteria = {
                        _id: payloadData.siteId
                    };
                    var setQuery = {
                        "$set" : {"siteInstructionArray" : managerObj.siteInstructionArray}
                    };
                    Service.SiteService.updateSite(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, managerObj.siteInstructionArray);
        })
    }
};


var deleteSiteImage = function (userData,payloadData, callback) {
    //console.log(payloadData);
    var managerObj = null,newSiteInstruction;
    if (!payloadData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    _id: payloadData.siteId
                };
                Service.SiteService.getSite(criteria, {}, {lean: true}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND);
                        } else {
                            managerObj = userData && userData[0] || null;
                            if(typeof managerObj.siteInstructionImages[payloadData.siteInstructionIndex] === 'undefined') {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND);
                            }
                            else {
                                console.log("before",managerObj.siteInstructionImages);
                                managerObj.siteInstructionImages.splice(payloadData.siteInstructionIndex,1);
                                console.log("after",managerObj.siteInstructionImages);
                                cb()
                            }

                        }
                    }
                })
            },
            function (cb) {
                if (managerObj) {
                    var criteria = {
                        _id: payloadData.siteId
                    };
                    var setQuery = {
                        "$set" : {"siteInstructionImages" : managerObj.siteInstructionImages}
                    };
                    Service.SiteService.updateSite(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, managerObj.siteInstructionImages);
        })
    }
};


var deleteSite = function (userData,queryData, callback) {
    var userId = null;
    if (!queryData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    _id: queryData.siteId
                };
                Service.SiteService.getSite(criteria,{},{}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            userId = userData[0] && userData[0]._id;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                //Finally Delete User
                var criteria = {
                    _id: userId
                };
                Service.SiteService.deleteSite(criteria, function (err, data) {
                    cb(err, data);
                })
            }
        ], function (err, result) {
            callback(err, null)

        })
    }
};



var assignGuardToSite = function (userData,payloadData, callback) {
    //console.log(payloadData);
    var managerObj = null,siteFound;
    if (!payloadData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: payloadData.siteId,
                    timeLotForGuard: {$elemMatch: {_id:payloadData.timeSlotId}}
                };
                var projection = {"timeLotForGuard.$":1};
                var option = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, option, function (err, result) {

                    if (err) {
                        cb(err)
                    } else {
                        siteFound = result && result[0] || null;
                        var arrayTime = siteFound.timeLotForGuard[0].guardId.map(String);
                        if(siteFound == null)
                        {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND)
                        } else {
                            var arrayTime = siteFound.timeLotForGuard[0].guardId.map(String);

                            if (siteFound.timeLotForGuard[0].guardId.length >= siteFound.timeLotForGuard[0].requiredGuard) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_ASSIGN)
                            } else if (arrayTime.indexOf(payloadData.guardId) > -1) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_ASSIGN_GUARD)
                            }
                            else {
                                cb();
                            }
                        }

                    }
                });

            },
            function (cb) {
                if (siteFound) {
                    var criteria = {
                        _id: payloadData.siteId,
                        timeLotForGuard: {$elemMatch: {_id:payloadData.timeSlotId}}
                    };
                    var setQuery = {
                        $push : {"timeLotForGuard.$.guardId" : payloadData.guardId}
                    };
                    Service.SiteService.updateSite(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};


var getSitePendingList = function (queryData, userData, callback) {
    var userFound = null,userCount= 0;
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria={managerId: userData.id,reportingLeftCount:{$gt:0},isArchive:false},
                    options = {
                        limit: queryData.limit || 0,
                        skip: queryData.skip || 0,
                        sort: {siteStartDate: 1}
                    },
                    populateVariable = {path: 'timeLotForGuard.guardId managerId',select:"name email"},
                    projection = {title:1,shortDescription:1,reportStatus:1,siteStartDate:1,reportingLeftCount:1,siteColorStatus:1};
                    Service.SiteService.getAndPopulateDataSite(criteria, projection, options ,populateVariable,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = data;
                        cb();
                    }
                })
            },
            function (cb) {
                var criteria={managerId: userData.id,reportingLeftCount:{$gt:0},isArchive:false};
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCount = data.length;
                        cb();

                    }
                })
            }
        ], function (err, result) {
            callback(err, {siteCount:userCount,siteList:userFound});
        })
    }
};



var getSiteArchiveList = function (queryData, userData, callback) {
    var userFound = null,userCount= 0;
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria={managerId: userData.id,isArchive:true},
                    options = {
                        limit: queryData.limit || 0,
                        skip: queryData.skip || 0,
                        sort: {siteStartDate: 1}
                    },
                    populateVariable = {path: 'timeLotForGuard.guardId managerId',select:"name email"},
                    projection = {title:1,shortDescription:1,reportStatus:1,siteStartDate:1,reportingLeftCount:1,siteColorStatus:1};
                Service.SiteService.getAndPopulateDataSite(criteria, projection, options ,populateVariable,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = data;
                        cb();
                    }
                })
            },
            function (cb) {
                var criteria={managerId: userData.id,isArchive:true};
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCount = data.length;
                        cb();

                    }
                })
            }
        ], function (err, result) {
            callback(err, {siteCount:userCount,siteList:userFound});
        })
    }
};


var makeSiteFlagReport = function (userData,payloadData, callback) {
    //console.log(payloadData);
    var managerObj = null,siteFound;
    if (!payloadData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: payloadData.siteId
                };
                var projection = {_id:1};
                var option = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, option, function (err, result) {

                    if (err) {
                        cb(err)
                    } else {
                        siteFound = result && result[0] || null;
                        if(siteFound == null)
                        {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND)
                        } else {
                                cb();
                        }

                    }
                });

            },
            function (cb) {
                var criteria = {
                    _id: payloadData.guardId
                };
                var projection = {_id:1,siteFlagReport:1};
                var option = {
                    lean: true
                };
                Service.GuardService.getGuard(criteria, projection, option, function (err, result) {

                    if (err) {
                        cb(err)
                    } else {
                        siteFound = result && result[0] || null;
                        if(siteFound == null)
                        {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND)
                        } else {
                            if(siteFound.siteFlagReport == true && payloadData.siteFlagReport == true){
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_REPORTED)

                            } else if(siteFound.siteFlagReport == false && payloadData.siteFlagReport == false) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_REPORTED_NOT)
                            } else {
                            cb();
                            }
                        }

                    }
                });

            },
            function (cb) {
                if (siteFound) {
                    var criteria = {
                        _id: payloadData.guardId
                    }
                };
                var setQuery = {
                    $set: {siteFlagReport: payloadData.siteFlagReport}
                };
                Service.GuardService.updateGuard(criteria, setQuery, {}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        cb();
                    }
                })
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};



var approveReportForSite = function (userData,payloadData, callback) {
    //console.log(payloadData);
    var managerObj = null,siteFound;
    if (!payloadData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: payloadData.siteId,
                    timeLotForGuard: {$elemMatch: {_id:{$in: payloadData.timeObjectId}}}
                };
                var projection = {"timeLotForGuard":1};
                var option = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, option, function (err, result) {

                    if (err) {
                        cb(err)
                    } else {
                        siteFound = result && result[0] || null;
                        var arrayTime = siteFound.timeLotForGuard[0].guardId.map(String);
                        if(siteFound == null)
                        {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.RECORD_NOT_FOUND)
                        } else {
                                cb();
                        }

                    }
                });

            },
            function(cb){
                var taskInParallel1 = [];
                if(payloadData.timeObjectId) {
                    for (var key in payloadData.timeObjectId) {
                        (function (key) {

                            taskInParallel1.push((function (key) {
                                return function (embeddedCB1) {//TODO

                                    console.log("payloadData.timeObjectId",payloadData.timeObjectId[key])
                                    var criteria = {
                                        _id: payloadData.siteId,
                                        timeLotForGuard: {$elemMatch: {_id: payloadData.timeObjectId[key]}}
                                    };

                                    var setQuery = {
                                        $set : {"timeLotForGuard.$.isReportingPending" : true}
                                    };

                                    var options = {

                                    };
                                    Service.SiteService.updateSite(criteria, setQuery, options,function (err, DataFromDB) {
                                        if (err) {
                                            embeddedCB1(err)
                                        } else {
                                            embeddedCB1();

                                        }
                                    })

                                }
                            })(key))
                        }(key));
                    }
                    async.parallel(taskInParallel1, function (err, result) {

                        if(err)
                        {
                            cb(err);
                        }
                        else {
                            cb();
                        }
                    });
                } else {
                    cb();
                }
            },
            function (cb) {
                if (siteFound) {
                    var criteria = {
                        _id: payloadData.siteId,
                    }
                };
                var setQuery = {
                 $inc : { "reportingLeftCount" : -payloadData.timeObjectId.length}
                };
                Service.SiteService.updateSite(criteria, setQuery, {}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        cb();
                    }
                })
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};





var getScheduleSiteListWeek = function (queryData, userData, callback) {
    var userFound = null,userCount= 0;
    if (queryData.weekFilterDate) {
        var startWeek = moment(queryData.weekFilterDate).startOf('isoWeek').toISOString();
        var endWeek = moment(queryData.weekFilterDate).endOf('isoWeek').toISOString();
    } else {
        var startWeek = moment().startOf('isoWeek').toISOString();
        var endWeek = moment().endOf('isoWeek').toISOString();
    }
    console.log("startWeek",startWeek,"endWeek",endWeek)
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria={
                    managerId: userData.id,
                    isArchive:false,
                    siteStartDate : {$gte: startWeek , $lte:  endWeek }
                    },
                    options = {
                        limit: queryData.limit || 0,
                        skip: queryData.skip || 0,
                        sort: {siteStartDate: -1}
                    },
                    populateVariable = {},
                    projection = {title:1,shortDescription:1,siteScheduleColorStatus:1,timeLotForGuard:1};
                    Service.SiteService.getSite(criteria, projection, options,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = data;
                        if(userFound.length > 0) {
                            for (var i = 0; i < userFound.length; i++) {
                                if (userFound[0].timeLotForGuard.length > 0){
                                    for (var j = 0; j < userFound[0].timeLotForGuard.length; j++) {
                                     //   console.log("--",i,j,userFound[i].timeLotForGuard[userFound[i].timeLotForGuard.length],userFound[i].timeLotForGuard.length)
                                    userFound[i]['startShift'] = userFound[i].timeLotForGuard[0].startTime;
                                    userFound[i]['endShift'] = userFound[i].timeLotForGuard[userFound[i].timeLotForGuard.length-1].endTime;

                                    }

                                }
                            }

                        }
                        cb();
                    }
                })
            },
            function (cb) {
                var criteria={managerId: userData.id,isArchive:false,  siteStartDate : {$gte: startWeek , $lte:  endWeek }};
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCount = data.length;
                        cb();

                    }
                })
            }
        ], function (err, result) {
            if(userFound.length > 0) { for (var i = 0; i < userFound.length; i++) {delete userFound[i].timeLotForGuard }}
            callback(err, {siteCount:userCount,siteList:userFound});
        })
    }
};

var getScheduleSiteListMonth = function (queryData, userData, callback) {
    var userFound = null,userCount= 0;
    if (queryData.weekFilterDate) {
        var startWeek =    moment().startOf('month').toString();
        var endWeek   =    moment().endOf("month").toString()
    } else {
        var startWeek =    moment(queryData.monthFilterDate).startOf('month').toString();
        var endWeek   =    moment(queryData.monthFilterDate).endOf("month").toString()
    }

    console.log("startWeek",startWeek,"endWeek",endWeek)
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria={
                        managerId: userData.id,
                        isArchive:false,
                        siteStartDate : {$gte: startWeek , $lte:  endWeek }
                    },
                    options = {
                        limit: queryData.limit || 0,
                        skip: queryData.skip || 0,
                        sort: {siteStartDate: -1}
                    },
                    populateVariable = {},
                    projection = {title:1,shortDescription:1,siteScheduleColorStatus:1,timeLotForGuard:1};
                Service.SiteService.getSite(criteria, projection, options,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = data;
                        if(userFound.length > 0) {
                            for (var i = 0; i < userFound.length; i++) {
                                if (userFound[0].timeLotForGuard.length > 0){
                                    for (var j = 0; j < userFound[0].timeLotForGuard.length; j++) {
                                        //   console.log("--",i,j,userFound[i].timeLotForGuard[userFound[i].timeLotForGuard.length],userFound[i].timeLotForGuard.length)
                                        userFound[i]['startShift'] = userFound[i].timeLotForGuard[0].startTime;
                                        userFound[i]['endShift'] = userFound[i].timeLotForGuard[userFound[i].timeLotForGuard.length-1].endTime;

                                    }

                                }
                            }

                        }
                        cb();
                    }
                })
            },
            function (cb) {
                var criteria={managerId: userData.id,isArchive:false,  siteStartDate : {$gte: startWeek , $lte:  endWeek }};
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCount = data.length;
                        cb();

                    }
                })
            }
        ], function (err, result) {
            if(userFound.length > 0) { for (var i = 0; i < userFound.length; i++) {delete userFound[i].timeLotForGuard }}
            callback(err, {siteCount:userCount,siteList:userFound});
        })
    }
};
module.exports = {
    addSite: addSite,
    deleteSiteInstruction:deleteSiteInstruction,
    deleteSiteImage:deleteSiteImage,
    deleteSite:deleteSite,
    assignGuardToSite:assignGuardToSite,
    getSitePendingList:getSitePendingList,
    getSiteArchiveList:getSiteArchiveList,
    makeSiteFlagReport:makeSiteFlagReport,
    approveReportForSite:approveReportForSite,
    getScheduleSiteListWeek:getScheduleSiteListWeek,
    getScheduleSiteListMonth:getScheduleSiteListMonth
}