'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');

function clearDeviceTokenFromDB(token, cb) {
    if (token) {
        var criteria = {
            deviceToken: token
        };
        var setQuery = {
            $unset: {deviceToken: 1}
        };
        var options = {
            multi: true
        };
        Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
    } else {
        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }
}

var createCustomer = function (payloadData, callback) {
    console.log('sending',payloadData)
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payloadData;
    if (payloadData.facebookId == 'undefined'){
        delete payloadData.facebookId;
    }
    if (payloadData.password == 'undefined'){
        delete payloadData.password;
    }
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    dataToSave.firstTimeLogin = false;
    var customerData = null;
    var dataToUpdate = {};
    if (payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate for facebookId and password
            if (dataToSave.facebookId) {
                if (dataToSave.password) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.FACEBOOK_ID_PASSWORD_ERROR);
                } else {
                    cb();
                }
            } else if (!dataToSave.password) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PASSWORD_REQUIRED);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            if (dataToSave.phoneNo && dataToSave.phoneNo.split('')[0] == 0) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            var criteria = {
                deviceToken: dataToSave.deviceToken
            };
            var setQuery = {
                $unset: {deviceToken: 1}
            };
            var options = {
                multi: true
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.newNumber = payloadData.phoneNo;
            dataToSave.registrationDate = new Date().toISOString();
            dataToSave.emailVerificationToken = UniversalFunctions.CryptData(JSON.stringify(dataToSave));
            Service.CustomerService.createCustomer(dataToSave, function (err, customerDataFromDB) {
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$phoneNo_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            if (customerData && customerData._id && payloadData.profilePic && payloadData.profilePic.filename) {
                UploadManager.uploadFileToS3WithThumbnail(payloadData.profilePic, customerData._id, function (err, uploadedInfo) {
                    console.log('update profile pic',err,uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
        function (cb) {
            if (customerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update User
                var criteria = {
                    _id: customerData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
                    customerData = updatedData;
                    cb(err, updatedData)
                })
            }else {
                if (customerData && customerData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original){
                    var criteria = {
                        _id: customerData._id
                    };
                    Service.CustomerService.deleteCustomer(criteria,function (err, updatedData) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                }else {
                    cb();
                }
            }
        },
        function (cb) {
            //clearDeviceTokenFromDB
            if (dataToSave.deviceToken && customerData) {
                clearDeviceTokenFromDB(dataToSave.deviceToken, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            //Send SMS to User
            if (customerData) {
                NotificationManager.sendSMSToUser(uniqueCode, dataToSave.countryCode, dataToSave.phoneNo, function (err, data) {
                    cb();
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Send Verification Email
            if (customerData) {
                var emailType = 'REGISTRATION_MAIL';
                var variableDetails = {
                    user_name: dataToSave.name,
                    verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/customer/verifyEmail/' + dataToSave.emailVerificationToken
                };
                var emailAddress = dataToSave.email;
                NotificationManager.sendEmailToUser(emailType, variableDetails, emailAddress, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Set Access Token
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(customerData)
            });
        }
    });
};

var loginCustomer = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(payloadData.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payloadData.password)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            if (userFound && payloadData.deviceToken != userFound.deviceToken && !flushPreviousSessions) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACTIVE_PREVIOUS_SESSIONS)
            } else {
                var criteria = {
                    deviceToken: payloadData.deviceToken
                };
                var setQuery = {
                    $unset: {deviceToken: 1}
                };
                var options = {
                    multi: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType,
                language: payloadData.language
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });

        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(updatedUserDetails)
            });
        }
    });
};

var loginCustomerViaFacebook = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    async.series([
        function (cb) {
            var criteria = {
                facebookId: payloadData.facebookId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.FACEBOOK_ID_NOT_FOUND);
            } else {
                successLogin = true;
                cb();
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            if (userFound && payloadData.deviceToken != userFound.deviceToken && !flushPreviousSessions) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACTIVE_PREVIOUS_SESSIONS)
            } else {
                var criteria = {
                    deviceToken: payloadData.deviceToken
                };
                var setQuery = {
                    $unset: {deviceToken: 1}
                };
                var options = {
                    multi: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType,
                language: payloadData.language
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });

        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(updatedUserDetails)
            });
        }
    });
};

var updateCustomer = function (userPayload, userData, callback) {
    var dataToUpdate = {};
    var updatedUser = null;
    var samePhoneNo = false;
    var uniqueCode = null;
    var phoneNoUpdateRequest = false;
    var newCountryCode = null;
    if (!userPayload || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        if (userPayload.phoneNo && userPayload.countryCode) {
            samePhoneNo = (userData.phoneNo == userPayload.phoneNo);
            samePhoneNo = (userData.countryCode == userPayload.phoneNo);
        }
        if (userPayload.name && userPayload.name != '') {
            dataToUpdate.name = UniversalFunctions.sanitizeName(userPayload.name);
        }
        if (userPayload.deviceToken && userPayload.deviceToken != '') {
            dataToUpdate.deviceToken = userPayload.deviceToken;
        }
        if (userPayload.phoneNo && userPayload.countryCode &&
            userData.phoneNo == userPayload.phoneNo && userData.countryCode == userPayload.countryCode) {
            delete userPayload.phoneNo;
            delete userPayload.countryCode;
        }

        if (userPayload.phoneNo && userPayload.phoneNo != '') {
            dataToUpdate.newNumber = userPayload.phoneNo;
        }
        if (userPayload.countryCode && userPayload.countryCode != '') {
            newCountryCode = userPayload.countryCode;
        }
        if (userPayload.email && userPayload.email != '') {
            dataToUpdate.email = userPayload.email;
        }
        if (userPayload.language && (userPayload.language == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN
            || userPayload.language == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX)) {
            dataToUpdate.language = userPayload.language;
        }
        if (userPayload.profilePic && userPayload.profilePic.filename) {
            dataToUpdate.profilePicURL = {
                original: null,
                thumbnail: null
            }
        }
        if (Object.keys(dataToUpdate).length == 0) {
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOTHING_TO_UPDATE);
        } else {
            async.series([
                function (cb) {
                    //verify email address
                    if (dataToUpdate.email && !UniversalFunctions.verifyEmailFormat(dataToUpdate.email)) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Validate phone No
                    if (userPayload.phoneNo && userPayload.phoneNo.split('')[0] == 0) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Validate countryCode and phoneNo
                    if (userPayload.countryCode) {
                        if (userPayload.countryCode.lastIndexOf('+') == 0) {
                            if (!isFinite(userPayload.countryCode.substr(1))) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                            }
                            else if (!userPayload.phoneNo || userPayload.phoneNo == '') {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                            } else {
                                cb();
                            }
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                        }
                    } else if (userPayload.phoneNo) {
                        if (!userPayload.countryCode) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                        } else if (userPayload.countryCode && userPayload.countryCode.lastIndexOf('+') == 0) {
                            if (!isFinite(userPayload.countryCode.substr(1))) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                            } else {
                                cb();
                            }
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                        }
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Check all empty values validations
                    if (userPayload.name && !dataToUpdate.name) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMPTY_VALUE)
                    } else if (dataToUpdate.name) {
                        UniversalFunctions.customQueryDataValidations('NAME', 'name', dataToUpdate.name, cb)
                    } else {
                        cb();
                    }

                },
                function (cb) {

                    //Check if profile pic is being updated
                    if (userPayload.profilePic && userPayload.profilePic.filename) {
                        UploadManager.uploadFileToS3WithThumbnail(userPayload.profilePic, userData.id, function (err, uploadedInfo) {
                            if (err) {
                                cb(err)
                            } else {
                                dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                                dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                                cb();
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (userPayload.countryCode && userPayload.phoneNo && !samePhoneNo) {
                        var criteria = {
                            countryCode: userPayload.countryCode,
                            phoneNo: userPayload.phoneNo
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            } else {
                                if (result && result.length > 0) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST)
                                } else {
                                    cb();
                                }
                            }
                        });
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (!samePhoneNo && userPayload.phoneNo) {
                        CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                            if (err) {
                                cb(err);
                            } else {
                                if (!numberObj || numberObj.number == null) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                                } else {
                                    uniqueCode = numberObj.number;
                                    cb();
                                }
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (newCountryCode && dataToUpdate.newNumber && !samePhoneNo) {
                        //Send SMS to User on the new number
                        UniversalFunctions.NotificationManager.sendSMSToUser(uniqueCode, newCountryCode, dataToUpdate.newNumber, function (err, data) {
                            dataToUpdate.OTPCode = uniqueCode;
                            dataToUpdate.newNumber = newCountryCode + '-' + dataToUpdate.newNumber;
                            phoneNoUpdateRequest = true;
                            cb();
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Update User
                    var criteria = {
                        _id: userData._id
                    };
                    var setQuery = {
                        $set: dataToUpdate
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
                        updatedUser = updatedData;
                        cb(err, updatedData)
                    })
                }
            ], function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, {
                        phoneNoUpdateRequest: phoneNoUpdateRequest,
                        userData: UniversalFunctions.deleteUnnecessaryUserData(updatedUser)
                    });
                }
            })
        }
    }
};

var resetPassword = function (payloadData, callback) {
    var customerObj = null;
    if (!payloadData || !payloadData.email || !payloadData.passwordResetToken || !payloadData.newPassword) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    email: payloadData.email
                };
                Service.CustomerService.getCustomer(criteria, {}, {lean: true}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData && userData[0] || null;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (customerObj) {
                    if (customerObj.passwordResetToken != payloadData.passwordResetToken) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RESET_PASSWORD_TOKEN);
                    } else {
                        cb();
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                }
            },
            function (cb) {
                if (customerObj) {
                    var criteria = {
                        email: payloadData.email
                    };
                    var setQuery = {
                        password: UniversalFunctions.CryptData(payloadData.newPassword),
                        $unset: {passwordResetToken: 1}
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var changePassword = function (queryData, userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.CustomerService.getCustomer(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                } else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                } else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin: false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};

var verifyEmail = function (emailVerificationToken, callback) {
    if (!emailVerificationToken) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    } else {
        var userData = null;
        async.series([
            function (cb) {
                var criteria = {
                    emailVerificationToken: emailVerificationToken
                };
                var setQuery = {
                    $unset: {emailVerificationToken: 1}
                };
                var options = {new: true};
                Service.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        } else {
                            userData = updatedData;
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null, UniversalFunctions.deleteUnnecessaryUserData(userData));
            }

        });
    }

};

var verifyOTP = function (queryData, userData, callback) {
    if (!queryData || !userData._id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    } else {
        var newNumberToVerify = queryData.countryCode + '-' + queryData.phoneNo;
        async.series([
            function (cb) {
                //Check verification code :
                if (queryData.OTPCode == userData.OTPCode) {
                    cb();
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                }
            },
            function (cb) {
                //Check if phoneNo is same as in DB
                console.log('checking data', userData.newNumber)
                console.log('checking data', newNumberToVerify)
                if (userData.newNumber != newNumberToVerify) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO);
                } else {
                    cb();
                }

            },
            function (cb) {
                //trying to update customer
                var criteria = {
                    _id: userData.id,
                    OTPCode: queryData.OTPCode
                };
                var setQuery = {
                    $set: {phoneVerified: true},
                    $unset: {OTPCode: 1}
                };
                if (userData.newNumber) {
                    setQuery.$set.phoneNo = queryData.phoneNo;
                    setQuery.$set.countryCode = queryData.countryCode;
                    setQuery.$unset.newNumber = 1;
                }
                var options = {new: true};
                console.log('updating>>>', criteria, setQuery, options)
                Service.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {
                    console.log('verify otp callback result', err, updatedData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        } else {
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback();
            }

        });
    }

};

var resendOTP = function (userData, callback) {
    /*
     Create a Unique 4 digit code
     Insert It Into Customer DB
     Send the 4 digit code via SMS
     Send Back Response
     */
    var phoneNo = userData.newNumber || userData.phoneNo;
    var countryCode = userData.countryCode;
    if (!phoneNo) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            function (cb) {
                CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode,
                        codeUpdatedAt: new Date().toISOString()
                    }
                };
                var options = {
                    lean: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }, function (cb) {
                //Send SMS to User
                NotificationManager.sendSMSToUser(uniqueCode, countryCode, phoneNo, function (err, data) {
                    cb();
                })
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var logoutCustomer = function (userData, callback) {
    if (!userData || !userData._id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userId = userData && userData._id || 1;

        async.series([
            function (cb) {
                //TODO Check Active Bookings Of Customer
                cb();
            },
            function (cb) {
                var criteria = {
                    _id: userId
                };
                var setQuery = {
                    $unset: {
                        accessToken: 1
                    }
                };
                var options = {};
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};

var getResetPasswordToken = function (email, callback) {
    var generatedString = UniversalFunctions.generateRandomString();
    var customerObj = null;
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //update user
                var criteria = {
                    email: email
                };
                var setQuery = {
                    passwordResetToken: UniversalFunctions.CryptData(generatedString)
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, userData) {
                    console.log('update user', err, userData, !userData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (customerObj) {
                    var variableDetails = {
                        user_name: customerObj.name,
                        password_reset_token: customerObj.passwordResetToken,
                        password_reset_link: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME +
                        + '/CustomerResetPassword.html?passwordResetToken=' + generatedString + '&email=' + customerObj.email
                    };
                    NotificationManager.sendEmailToUser('FORGOT_PASSWORD', variableDetails, customerObj.email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null, {password_reset_token: customerObj.passwordResetToken})//TODO Change in production DO NOT Expose the password
            }
        })
    }
};

var addNewAddress = function (payloadData, userData, callback) {
    if (!payloadData || !userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userDataToSend = null;
        async.series([
            function (cb) {
                payloadData.locationLongLat = [payloadData.locationLong,payloadData.locationLat];
                payloadData.customer = userData.id;
                Service.CustomerService.addAddress(payloadData, function (err, userAddData) {
                    if (err) {
                        cb(err)
                    } else {
                        cb()
                    }
                })
            },
            function (cb) {
                Service.CustomerService.getAddress({
                    customer: userData.id,
                    isDeleted: false
                }, {}, {lean: true}, function (err, userAddArray) {
                    if (err) {
                        cb(err)
                    } else {
                        userDataToSend = userAddArray;
                        cb();
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                callback(err)
            } else {
                callback(null, userDataToSend)
            }
        });

    }
};

var getAddress = function (userData, callback) {
    if (!userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userDataToSend = null;

        async.series([
            function (cb) {
                var criteria = {
                    customer: userData.id,
                    isDeleted: false
                };
                Service.CustomerService.getAddress(criteria, {}, {lean: true}, function (err, userAddArray) {
                    if (err) {
                        cb(err)
                    } else {
                        userDataToSend = userAddArray;
                        cb();
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                callback(err)
            } else {
                callback(null, userDataToSend)
            }
        });

    }

};

var removeAddress = function (addressId, userData, callback) {
    if (!userData || !userData.id || !addressId) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    isDeleted : false,
                    customer: userData.id,
                    _id: addressId
                };
                Service.CustomerService.updateAddress(criteria, {isDeleted:true}, {}, function (err, userAddArray) {
                    if (err) {
                        cb(err)
                    } else {
                        if (userAddArray){
                            cb();
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ADDRESS_NOT_FOUND);
                        }
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                callback(err)
            } else {
                callback(null,null)
            }
        });

    }

};

module.exports = {
    createCustomer: createCustomer,
    logoutCustomer: logoutCustomer,
    addNewAddress: addNewAddress,
    getAddress: getAddress,
    removeAddress: removeAddress,
    verifyEmail: verifyEmail,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    loginCustomerViaFacebook: loginCustomerViaFacebook,
    getResetPasswordToken: getResetPasswordToken,
    loginCustomer: loginCustomer,
    changePassword: changePassword,
    resetPassword: resetPassword,
    updateCustomer: updateCustomer
};