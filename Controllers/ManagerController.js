'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var Config = require('../Config');
var moment = require('moment');

var UploadManager = require('../Lib/UploadManager');
var TokenManager = require('../Lib/TokenManager');
var NotificationManager = require('../Lib/NotificationManager');
var CodeGenerator = require('../Lib/CodeGenerator');

function clearDeviceTokenFromDB(token,cb){
    if (token) {
        var criteria = {
            deviceToken: token
        };
        var setQuery = {
            $unset: {deviceToken: 1}
        };
        var options = {
            multi: true
        };
        Service.ManagerService.updateManager(criteria, setQuery, options, cb)
    } else {
        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }
}

var createManager = function (payloadData, callback) {
    console.log('sending',payloadData);
    payloadData.facebookId = UniversalFunctions.sanitizeName(payloadData.facebookId);
    var accessToken = null;
    var uniqueCode = null;
    var dataToSave = payloadData;
    if (payloadData.facebookId == 'undefined'){
        delete payloadData.facebookId;
    }
    if (payloadData.password == 'undefined'){
        delete payloadData.password;
    }
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    dataToSave.firstTimeLogin = false;
    var ManagerData = null;
    var dataToUpdate = {};
    if (payloadData.profilePic && payloadData.profilePic.filename) {
        dataToUpdate.profilePicURL = {
            original: null,
            thumbnail: null
        }
    }

    async.series([
        function (cb) {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate for facebookId and password
            console.log('datadataToSave.facebookId',dataToSave.facebookId)
            if (~~(dataToSave.facebookId)&&dataToSave.facebookId && dataToSave.facebookId.length > 0) {
                console.log('datadataToSave.facebookId',dataToSave.facebookId)

                if (dataToSave.password) {
                    console.log('datadataToSave.facebookId',dataToSave.facebookId)

                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.FACEBOOK_ID_PASSWORD_ERROR);
                } else {
                    cb();
                }
            } else if (!dataToSave.password) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PASSWORD_REQUIRED);
            } else {
                cb();
            }
        },
        function (cb) {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                }
                else {
                    cb();
                }
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        function (cb) {
            //Validate phone No
            console.log('dataToSave.phoneNo',dataToSave.phoneNo)
            if (dataToSave.phoneNo && dataToSave.phoneNo.split('')[0] == 0) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        function (cb) {
            CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.Manager, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            var criteria = {
                deviceToken: dataToSave.deviceToken
            };
            var setQuery = {
                $unset: {deviceToken: 1}
            };
            var options = {
                multi: true
            };
            Service.ManagerService.updateManager(criteria, setQuery, options, cb)
        },
        function (cb) {
            //Insert Into DB
            dataToSave.OTPCode = uniqueCode;
            dataToSave.registrationDate = new Date().toISOString();
            dataToSave.emailVerificationToken = UniversalFunctions.CryptData(JSON.stringify(dataToSave));
            Service.ManagerService.createManager(dataToSave, function (err, dataFromDB) {
                console.log('customeized erro',err,dataFromDB)
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('Managers.$phoneNo_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('Managers.$email_1') > -1){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                    }else {
                        cb(err)
                    }
                } else {
                    console.log('setting Manager',ManagerData)
                    ManagerData = dataFromDB;
                    console.log('setting Manager',ManagerData)
                    cb();
                }
            })
        },
        function (cb) {
            //Check if profile pic is being updated
            if (ManagerData && ManagerData._id && payloadData.profilePic && payloadData.profilePic.filename) {
                UploadManager.uploadFileToS3WithThumbnail(payloadData.profilePic, ManagerData._id, function (err, uploadedInfo) {
                    console.log('update profile pic',err,uploadedInfo)
                    if (err) {
                        cb(err)
                    } else {
                        dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        },
        function (cb) {
            if (ManagerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
                //Update User
                var criteria = {
                    _id: ManagerData._id
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.ManagerService.updateManager(criteria, setQuery, {new: true}, function (err, updatedData) {
                    ManagerData = updatedData;
                    cb(err, updatedData)
                })
            }else {
                if (ManagerData && ManagerData._id && payloadData.profilePic && payloadData.profilePic.filename && !dataToUpdate.profilePicURL.original){
                    var criteria = {
                        _id: ManagerData._id
                    };
                    Service.ManagerService.deleteManager(criteria,function (err, updatedData) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
                    })
                }else {
                    cb();
                }
            }
        },
        function (cb) {
            //clearDeviceTokenFromDB
            console.log('payloadData.deviceToken && ManagerData',payloadData.deviceToken , ManagerData)
            if (payloadData.deviceToken && ManagerData) {
                clearDeviceTokenFromDB(payloadData.deviceToken, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            //Send SMS to User
            if (ManagerData) {
                NotificationManager.sendSMSToUser(uniqueCode, dataToSave.countryCode, dataToSave.phoneNo, function (err, data) {
                    cb();
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Send Verification Email
            if (ManagerData) {
                var emailType = 'REGISTRATION_MAIL';
                var variableDetails = {
                    user_name: dataToSave.name,
                    verification_url: UniversalFunctions.CONFIG.APP_CONSTANTS.SERVER.DOMAIN_NAME + '/api/Manager/verifyEmail/' + dataToSave.emailVerificationToken
                };
                var emailAddress = dataToSave.email;
                NotificationManager.sendEmailToUser(emailType, variableDetails, emailAddress, cb)
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }

        },
        function (cb) {
            //Set Access Token
            if (ManagerData) {
                var tokenData = {
                    id: ManagerData._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.Manager
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: UniversalFunctions.deleteUnnecessaryUserData(ManagerData)
            });
        }
    });
};

var loginManager = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var flushPreviousSessions = payloadData.flushPreviousSessions || false;
    var updatedUserDetails = null;
    async.series([
        function (cb) {
            var criteria = {
                email: payloadData.email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.ManagerService.getManager(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
            } else {
                if (userFound && userFound.password != UniversalFunctions.CryptData(payloadData.password)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_PASSWORD);
                } else {
                    successLogin = true;
                    cb();
                }
            }
        },
        function (cb) {
            //Clear Device Tokens if present anywhere else
            if (userFound && payloadData.deviceToken != userFound.deviceToken && !flushPreviousSessions){
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ACTIVE_PREVIOUS_SESSIONS)
            } else {
                var criteria = {
                    deviceToken: payloadData.deviceToken
                };
                var setQuery = {
                    $unset: {deviceToken: 1}
                };
                var options = {
                    multi: true
                };
                Service.ManagerService.updateManager(criteria, setQuery, options, cb)
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            var setQuery = {
                appVersion: payloadData.appVersion,
                deviceToken: payloadData.deviceToken,
                deviceType: payloadData.deviceType
            };
            Service.ManagerService.updateManager(criteria, setQuery, {new:true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });

        },
        function (cb) {
            if (successLogin) {
                var tokenData = {
                    id: userFound._id,
                    type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.MANAGER
                };
                TokenManager.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken){
                            accessToken = output && output.accessToken;
                            cb();
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
            }

        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {accessToken: accessToken, userDetails: UniversalFunctions.deleteUnnecessaryUserData(updatedUserDetails)});
        }
    });
};

var logoutManager = function (userData, callback) {
    if (!userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userId = userData && userData.id || 1;

        async.series([
            function (cb) {
                //Check if the Manager is free or not
                Service.ManagerService.getManager({_id : userData.id}, {availabilityStatus : 1},{lean:true}, function (err, ManagerAry) {
                    if (err){
                        cb(err)
                    }else if (ManagerAry && ManagerAry[0]){
                        if (ManagerAry[0].availabilityStatus){
                            cb()
                        }else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.CANNOT_LOGOUT)
                        }
                    }else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userId
                };
                var setQuery = {
                    $unset: {
                        accessToken: 1
                    }
                };
                var options = {};
                Service.ManagerService.updateManager(criteria, setQuery, options, cb);
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};


var firstTimeChangePassword = function (queryData, userData, callback) {
    var userFound = null;
    if (!queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.ManagerService.getManager(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            console.log("sdfsdfsdfs",userFound)
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.firstTimeLogin == true) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ALREADY_CHANGED)
                } else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin: true,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.ManagerService.updateManager(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};


var changePassword = function (queryData, userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.ManagerService.getManager(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                } else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                } else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.ManagerService.updateManager(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};

var tokenLoginManager = function (userPayload, userData, callback) {

    var dataToUpdate =  userPayload;
    var updatedUser,successLogin = null;
    if (!userPayload || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //validations
                if (!userData) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_NOT_FOUND);
                } else {
                    if(userData.isBlocked == true) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.BLOCKED);
                    } else if(userData.isDeleted == true) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.DELETED);
                    } else {
                        successLogin = true;
                        cb();
                    }
                }
            },
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null, {
                    userData: UniversalFunctions.deleteUnnecessaryUserData(userData)
                });
            }
        })

    }
};



var getAllSiteList = function (queryData, userData, callback) {
    var userFound = null,userCount= 0;
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([

            function (cb) {
                var criteria={managerId: userData.id,isArchive:false},
                  options = {
                    limit: queryData.limit || 0,
                    skip: queryData.skip || 0,
                    sort: {siteStartDate: 1}
                     },
                    populateVariable = {path: 'timeLotForGuard.guardId managerId',select:"name email"},
                    projection = {title:1,shortDescription:1,reportStatus:1,siteStartDate:1,reportingLeftCount:1,siteColorStatus:1};
                    Service.SiteService.getAndPopulateDataSite(criteria, projection, options ,populateVariable,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                            userFound = data;
                            cb();
                    }
                })
            },
            function (cb) {
                var criteria = {
                    managerId: userData.id,isArchive:false
                };
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                            userCount = data.length;
                            cb();

                    }
                })
            }
        ], function (err, result) {
            callback(err, {siteCount:userCount,siteList:userFound});
        })
    }
};



var getSiteDetails = function (queryData, userData, callback) {
    var userFound = null,userCount= 0,userCountYellow= 0,userCountRed =0,userCountGreen= 0;
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {

                var criteria={_id: queryData.siteId},
                    options = {
                    },
                    populateVariable = {path: 'timeLotForGuard.guardId managerId',select:"name email countryCode phoneNo siteColorStatus profilePicURL siteFlagReport"},
                    projection = {};
                    Service.SiteService.getAndPopulateDataSite(criteria, projection, options ,populateVariable,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = data;
                        cb();
                    }
                })
            },
        ], function (err, result) {
            callback(err, userFound);
        })
    }
};

var getCurrentSiteList = function (queryData, userData, callback) {
    var nowDate = moment().utc().startOf('day').toISOString();
    var getNextDate = moment(nowDate).add(1, 'days').toISOString();    // add one day

    var userFound = null,userCount= 0,userCountYellow= 0,userCountRed =0,userCountGreen= 0;
    if (!queryData  || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {

                var criteria={managerId: userData.id,isArchive:false},
                    options = {
                        limit: queryData.limit || 0,
                        skip: queryData.skip || 0,
                        sort: {siteStartDate: 1}
                    },
                    populateVariable = {path: 'timeLotForGuard.guardId managerId',select:"name email"},
                    projection = {guardId:1, managerId:1,title:1,shortDescription:1,contactNumber:1,siteStartDate:1};
                Service.SiteService.getAndPopulateDataSite(criteria, projection, options ,populateVariable,function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        userFound = data;
                        cb();
                    }
                })
            },
            function (cb) {
                var criteria = {
                    managerId: userData.id
                };
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCount = data.length;
                        cb();

                    }
                })
            },
            function (cb) {
                var criteria = {
                    managerId: userData.id,
                    siteColorStatus:Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.GREEN,
                    siteStartDate:{$gte:nowDate,$lt:getNextDate}
                };
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCountGreen = data.length;
                        cb();

                    }
                })
            },
            function (cb) {
                var criteria = {
                    managerId: userData.id,
                    siteColorStatus:Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.RED,
                    siteStartDate:{$gte:nowDate,$lt:getNextDate}
                };
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCountRed = data.length;
                        cb();

                    }
                })
            },
            function (cb) {
                var criteria = {
                    managerId: userData.id,
                    siteColorStatus:Config.APP_CONSTANTS.DATABASE.SITE_COLOR_STATUS.YELLOW,
                    siteStartDate:{$gte:nowDate,$lt:getNextDate}
                };
                var projection = {_id:1};
                var options = {
                    lean: true
                };
                Service.SiteService.getSite(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {

                        userCountYellow = data.length;
                        cb();

                    }
                })
            },


        ], function (err, result) {
            callback(err, {siteCount:userCount,siteCountGreen:userCountGreen,siteCountYellow:userCountYellow,
                siteCountRed:userCountYellow,siteList:userFound});
        })
    }
};


module.exports = {
    loginManager: loginManager,
    firstTimeChangePassword:firstTimeChangePassword,
    changePassword:changePassword,
    tokenLoginManager:tokenLoginManager,
    logoutManager:logoutManager,
    getSiteList:getAllSiteList,
    getSiteDetails:getSiteDetails,
    getCurrentSiteList:getCurrentSiteList


};