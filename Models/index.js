/**
 * Created by shahab on 10/7/15.
 */
module.exports = {
    Customers : require('./Customers'),
    CustomerAddresses : require('./CustomerAddresses'),
    Admins : require('./Admins'),
    AppVersions : require('./AppVersions'),
    Manager : require('./Manager'),
    Guard:require('./Guard'),
    Site:require('./Sites')
};