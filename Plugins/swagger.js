
//Register Swagger
const pack = require('../package'),
    swaggerOptions = {
        //  basePath : '/api/v1',
        pathPrefixSize: 2,
        info : {
            version : pack.version
        }

    };

const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');

exports.register = function(server, options, next){

    server.register([
        Inert,
        Vision,
        {
            'register': HapiSwagger,
            'options': swaggerOptions
        }], (err) => {
        if (err){
        console.log('Error Loading Swagger : ' + err)
    }
});

next();
};

exports.register.attributes = {
    name: 'swagger-plugin'
};