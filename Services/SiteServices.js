'use strict';

var Models = require('../Models');

//Get Users from DB
var getSite = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.Site.find(criteria, projection, options, callback);
};

//Insert User in DB
var createSite = function (objToSave, callback) {
    new Models.Site(objToSave).save(callback)
};

//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.Site.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};

//Update User in DB
var updateSite = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.Site.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteSite= function (criteria, callback) {
    Models.Site.findOneAndRemove(criteria, callback);
};

var getAndPopulateDataSite = function (query,projection,option,populateQuery, callback) {

    Models.Site.find(query, projection, option).populate(populateQuery).exec( function (err, data) {

        if (err) {

            callback(err);
        }
        else {
            callback(null, data);
        }
    })
};


module.exports = {
    getSite: getSite,
    getAllGeneratedCodes: getAllGeneratedCodes,
    updateSite: updateSite,
    deleteSite: deleteSite,
    createSite: createSite,
    getAndPopulateDataSite:getAndPopulateDataSite
};