'use strict';
var mongoURL

if (process.env.NODE_ENV == 'local') {

    mongoURL = "mongodb://localhost/novagems_local";

} else if (process.env.NODE_ENV == 'dev') {

    mongoURL = 'mongodb://novagems_dev:5yXubS7qxVp6sp@localhost/novagems_dev';

} else if (process.env.NODE_ENV == 'live') {

    mongoURL = "mongodb://localhost/novagems_local";

} else {
    mongoURL = "mongodb://localhost/novagems_local";
}


var mongo = {
    URI: mongoURL,
    port: 27017
};


module.exports = {
    mongo: mongo
};