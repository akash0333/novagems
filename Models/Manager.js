var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Manager = new Schema({
    name: {type: String, trim: true, index: true, default: null, sparse: true},
    countryCode: {type: String, required: true, trim: true, min: 1, max: 4},
    phoneNo: {type: String, required: true, trim: true, index: true, unique: true, min: 8, max: 15},
    facebookId: {type: String, default: null, trim: true, index: true},
    email: {type: String, trim: true, unique: true, index: true},
    DOB : {type: Date},
    accessToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    registrationCompleted: {type: Boolean, required: true, default:false},
    password: {type: String, required: true},
  //  passwordResetToken: {type: String, trim: true, unique: true, index: true},
    registrationDate: {type: Date, default: Date.now, required: true},
    appVersion: {type: String},
    deviceToken: {type: String, trim: true, index: true, unique: true, sparse: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    isBlocked: {type: Boolean, default: false, required: true},
    isDeleted: {type: Boolean, default: false, required: true},
    firstTimeLogin: {type: Boolean, default: false, required: true},
    currentLocation: {type: [Number], index: '2d'},
    address: {type: String, default: null},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    }
});
Manager.index({'currentLocation.coordinates': "2d"});


module.exports = mongoose.model('Manager', Manager);